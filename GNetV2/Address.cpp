#include "Address.h"

Address::Address()
{
	int a = 0;
	int b = 0;
	int c = 0;
	int d = 0;
	this->address = (a << 24) | (b << 16) | (c << 8) | d;
	this->port = 0;
}

Address::Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port)
{
	this->address = (a << 24) | (b << 16) | (c << 8) | d;
	this->port = port;
}

Address::Address(unsigned int address, unsigned short port)
{
	this->address = address;
	this->port = port;
}

unsigned int Address::GetAddress() const
{
	return address;
}

unsigned short Address::GetPort() const
{
	return port;
}

std::string Address::toString() const
{
	char buffer[22];
	sprintf(buffer, "%d.%d.%d.%d:%d", (uint8_t)(address >> 24), (uint8_t)(address >> 16), (uint8_t)(address >> 8), (uint8_t)(address >> 0), port);
	return std::string(buffer);
}

bool Address::operator==(const Address& other) const
{
	return this->address == other.address && this->port == other.port;
}

bool Address::operator!=(const Address& other) const
{
	return this->address != other.address || this->port != other.port;
}
