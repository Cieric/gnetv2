#pragma once
#include <stdint.h>

template < size_t size >
inline void sized_byteswap(char* data);

template <>
inline void sized_byteswap<2>(char* data)
{
	uint16_t* ptr = reinterpret_cast<uint16_t*>(data);
	*ptr = (*ptr >> 8) | (*ptr << 8);
}

template <>
inline void sized_byteswap<4>(char* data)
{
	uint32_t* ptr = reinterpret_cast<uint32_t*>(data);
	*ptr = (*ptr >> 24) | ((*ptr & 0x00ff0000) >> 8) | ((*ptr & 0x0000ff00) << 8) | (*ptr << 24);
}

template < typename T >
T byteswap(T value)
{
	sized_byteswap< sizeof(T) >(reinterpret_cast<char*>(&value));
	return value;
}