#include "Connection.h"
#include <assert.h>
#include <cstring>
#include <algorithm>
#include <unordered_set>
#include "Utils.h"

Connection::Connection() : log(16)
{
	log.enqueue("Client started...");
}

bool Connection::Connected()
{
	using namespace std::chrono;
	double length = duration_cast<duration<double>>(high_resolution_clock::now() - lastRecv).count();
	return length < 10.0;
}

bool Connection::Listen(unsigned short port)
{
	socket.Open(port);
	while (true)
	{
		Address sender;
		unsigned char buffer[256];
		int bytes_read = socket.Receive(sender, buffer, sizeof(buffer));
		if (!bytes_read) continue;

		if (memcmp((char*)buffer, "GNET", 4) == 0) {
			address = sender;
			printf("Connection established with %s!\n", address.toString().c_str());
			lastRecv = std::chrono::high_resolution_clock::now();
			send_time[1024 - 1] = lastRecv;
			return true;
		}
	}
}

bool Connection::Connect(const Address& destination)
{
	socket.Open(0);
	socket.Send(destination, "GNET", 4);
	address = destination;
	lastRecv = std::chrono::high_resolution_clock::now();
	send_time[1024 - 1] = lastRecv;
	return true;
}

bool Connection::Send(const void* data, int size)
{
	assert(size < (256 - sizeof(GamePacketHeader)));
	unsigned char buffer[256] = {};
	GamePacketHeader* header = (GamePacketHeader*)buffer;
	header->protocolId = GameProtocolId;
	header->sequence = localSequence++;
	header->ack = remoteSequence;
	header->ackBitfield = packet_recved;
	memcpy(buffer + sizeof(GamePacketHeader), data, std::min<size_t>(size, 256 - sizeof(GamePacketHeader)));
	auto now = std::chrono::high_resolution_clock::now();
	send_time[header->sequence % 1024] = now;
	return socket.Send(address, buffer, size + sizeof(GamePacketHeader));
}

std::string Format(const char* format, ...)
{
	static char* buffer = nullptr;
	static size_t buffersize = 0;

	va_list argptr;
	va_start(argptr, format);
	int size = vsnprintf(NULL, 0, format, argptr);
	if (buffersize < size)
	{
		void* temp = realloc(buffer, size);
		if (temp == nullptr) return "";
		buffersize = size;
		buffer = (char*)temp;
	}
	vsprintf(buffer, format, argptr);
	va_end(argptr);
	return std::string(buffer);
}

void Connection::ProcessAcks(uint16_t ack, uint32_t ackBitfield)
{
	using namespace std::chrono;
	auto now = std::chrono::high_resolution_clock::now();
	for (int i = 31; i >= 0; i--)
	{
		if ((ackBitfield >> i) & 0x1)
		{
			uint16_t seq = ack - 1 - i;
			auto sent_time = send_time[(seq + 1) % 1024];
			int time = int(duration_cast<milliseconds>(now - sent_time).count());
			log.enqueue(Format("Acked packet#%hu, RTT=%dms", seq, time));
			avgRTT *= 0.9;
			avgRTT += double(time) * 0.1;
		}
	}
}

inline bool seq_gt(uint16_t s1, uint16_t s2)
{
	return ((s1 > s2) && (s1 - s2 <= 32768)) || ((s1 < s2) && (s2 - s1 > 32768));
}

int Connection::Receive(void* data, int size, bool blocking)
{
	using namespace std::chrono;

	do {
		Address sender;
		unsigned char buffer[256];
		int bytes_read = socket.Receive(sender, buffer, 256);
		if (bytes_read < 0) return bytes_read;
		if (!bytes_read) continue;

		GamePacketHeader* header = (GamePacketHeader*)buffer;
		if (header->protocolId != GameProtocolId)
			continue;
		
		int16_t difference = header->sequence - remoteSequence;
		if (difference <= 0) {
			packet_recved |= 1 >> (difference+1);
			continue;
		};

		int16_t ackDiff = header->ack - last_ack;
		if(ackDiff >= 0)
		{
			uint32_t ackBitfield = header->ackBitfield;
			uint32_t newAcked = ackBitfield & ~(packet_acked << ackDiff);
			ProcessAcks(header->ack, newAcked);
			packet_acked = ackBitfield;
			last_ack = header->ack;
		}

		remoteSequence = header->sequence;
		packet_recved = (packet_recved << 1) | 1;
		packet_recved <<= difference-1;

		if (sender != address) continue;
		lastRecv = std::chrono::high_resolution_clock::now();
		memcpy(data, buffer + sizeof(GamePacketHeader), std::min<size_t>(size, 256 - sizeof(GamePacketHeader)));
		return bytes_read - sizeof(GamePacketHeader);
	} while (Connected() && blocking);
	bool conn = Connected();
	return conn ? 0 : -1;
}

Pipe& Junction::AddPipe(std::string name, Pipe::Options options)
{
	Pipe pipe = Pipe();
	pipe.options = options;
	return pipes[name] = std::move(pipe);
}
