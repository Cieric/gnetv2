#pragma once
#include <deque>
#include <chrono>
#include <unordered_map>
#include <string>
#include "Address.h"
#include "Socket.h"
#include "EndianTypes.h"
#include "Circular_Buffer.h"

static const uint32_t GameProtocolId = 0x7d98ee63;

#pragma pack(push,1)
struct GamePacketHeader
{
	be_uint32_t protocolId;
	uint16_t sequence;
	uint16_t ack;
	be_uint32_t ackBitfield;
};
#pragma pack(pop)

struct Pipe
{
	enum class Options
	{
		Reliable = 1, //TODO: Implement
		Sequenced = 2, //TODO: Implement
		AutoFragment = 4, //TODO: Implement
		AutoPack = 8, //TODO: Implement
		Encrypt = 16, //TODO: Implement
	};
	Options options;
};

class Junction
{
	Socket socket;
	std::unordered_map<std::string, Pipe> pipes;
public:
	Pipe& AddPipe(std::string name, Pipe::Options options);
};

typedef std::chrono::high_resolution_clock::time_point time_point;

struct Connection
{
	Address address;
	Junction junction;
	time_point lastRecv;

	uint16_t localSequence = 0;
	uint16_t remoteSequence = 0;
	uint32_t packet_recved = ~uint32_t(0);

	uint16_t last_ack = 0;
	uint32_t packet_acked = ~uint32_t(0);

	time_point send_time[1024];
	double avgRTT = 0.0;

	Circular_Buffer<std::string> log;

	void ProcessAcks(uint16_t ack, uint32_t ackBitfield);

public:
	Connection();

	bool Connected();
	bool Listen(unsigned short port);
	bool Connect(const Address& destination);
	bool Send(const void* data, int size);
	int Receive(void* data, int size, bool blocking = true);
};
