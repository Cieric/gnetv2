#include "Server.h"

int Server::FindFreeClientIndex() const
{
    for (int i = 0; i < m_maxClients; ++i)
    {
        if (!m_clientConnected[i])
            return i;
    }
    return -1;
}

int Server::FindExistingClientIndex(const Address& address) const
{
    for (int i = 0; i < m_maxClients; ++i)
    {
        if (m_clientConnected[i] && m_clientAddress[i] == address)
            return i;
    }
    return -1;
}

bool Server::IsClientConnected(int clientIndex) const
{
    return m_clientConnected[clientIndex];
}

const Address& Server::GetClientAddress(int clientIndex) const
{
    return m_clientAddress[clientIndex];
}