#pragma once
#include "Address.h"

const int MaxClients = 64;

class Server
{
    int m_maxClients;
    int m_numConnectedClients;
    bool m_clientConnected[MaxClients];
    Address m_clientAddress[MaxClients];
public:
    int FindFreeClientIndex() const;
    int FindExistingClientIndex(const Address& address) const;
    bool IsClientConnected(int clientIndex) const;
    const Address& GetClientAddress(int clientIndex) const;
};