#include "Socket.h"

#include <iostream>
#include <cstring>
#include <algorithm>

#if PLATFORM == PLATFORM_WINDOWS
#pragma comment(lib, "Ws2_32.lib")
#endif

bool InitializeSockets()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSADATA WsaData;
	return WSAStartup(MAKEWORD(2, 2), &WsaData) == NO_ERROR;
#else
	return true;
#endif
}

void ShutdownSockets()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSACleanup();
#endif
}

Socket::Socket() : handle(INVALID_SOCKET), open(false) {}

Socket::~Socket()
{
	if (IsOpen())
		Close();
}

bool Socket::Open(unsigned short port)
{
	handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (handle <= 0 || handle == INVALID_SOCKET)
	{
		printf("Error: failed to create socket (%d, %d)\n", handle, Error());
		open = false;
		return false;
	}

	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons((unsigned short)port);

	if (bind(handle, (const sockaddr*)& address, sizeof(sockaddr_in)) < 0)
	{
		printf("Error: failed to bind socket\n");
		open = false;
		return false;
	}

#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
	int nonBlocking = 1;
	if (fcntl(handle, F_SETFL, O_NONBLOCK, nonBlocking) == -1)
	{
		printf("Error: failed to set non-blocking\n");
		return (open = false);
	}
#elif PLATFORM == PLATFORM_WINDOWS
	DWORD nonBlocking = 1;
	if (ioctlsocket(handle, FIONBIO, &nonBlocking) != 0)
	{
		printf("Error: failed to set non-blocking\n");
		open = false;
		return false;
	}
#endif

	open = true;
	return true;
}

void Socket::Close()
{
#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
	close(handle);
#elif PLATFORM == PLATFORM_WINDOWS
	closesocket(handle);
#endif
}

bool Socket::IsOpen() const
{
	return open;
}

bool Socket::Send(const Address& destination, const void* data, int size)
{
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(destination.GetAddress());
	address.sin_port = htons(destination.GetPort());

	int sent_bytes = sendto(handle, (const char*)data, size, 0, (sockaddr*)& address, sizeof(sockaddr_in));
	if (sent_bytes != size)
	{
		printf("Error: failed to send packet (%d, %d)\n", sent_bytes, Error());
		return false;
	}

	return true;
}

int Socket::Available()
{
	u_long bytes_available;
#if PLATFORM == PLATFORM_WINDOWS
	ioctlsocket(handle, FIONREAD, &bytes_available);
#else
	ioctl(handle, FIONREAD, &bytes_available);
#endif
	return bytes_available;
}

int Socket::Error()
{
#if PLATFORM == PLATFORM_WINDOWS
	int err = WSAGetLastError();
	switch (err)
	{
	case WSAEMSGSIZE: return EFAULT;
	case WSAEWOULDBLOCK: return EWOULDBLOCK;
	case WSAECONNRESET: return ECONNRESET;
	}
	return err;
#else
	return errno;
#endif
}

int Socket::Receive(Address& sender, void* data, int size, bool blocking)
{
	do {
		unsigned char packet_data[256];
		unsigned int max_packet_size = sizeof(packet_data);
#if PLATFORM == PLATFORM_WINDOWS
		typedef int socklen_t;
#endif

		sockaddr_in from;
		socklen_t fromLength = sizeof(from);

		int bytes = recvfrom(handle, (char*)packet_data, max_packet_size, 0, (sockaddr*)& from, &fromLength);
		
		if (bytes < 0)
		{
			int err = Error();
			if (err == EFAULT) {
				printf("Packet to large, dropped.\n");
				continue;
			};
			if (err != EWOULDBLOCK)
			{
				printf("Packet dropped, %d\n", err);
				return -1;
			}
			return 0;
		}
		else if (bytes == 0)
		{
			return 0;
		}

		unsigned int from_address = ntohl(from.sin_addr.s_addr);
		unsigned int from_port = ntohs(from.sin_port);

		sender = Address(from_address, from_port);
		size = std::min(size, bytes);
		memcpy(data, packet_data, size);

		return size;
	} while (blocking);
	return 0;
}