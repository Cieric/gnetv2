#pragma once
#include "Platform.h"
#include "Address.h"

#if PLATFORM == PLATFORM_WINDOWS
#include <WinSock2.h>
#elif PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#define INVALID_SOCKET ((socket_t)(~0))
#endif

bool InitializeSockets();
void ShutdownSockets();

class Socket
{
	using socket_t = decltype(socket(0, 0, 0));
public:
	Socket();
	~Socket();

	bool Open(unsigned short port = 0);
	void Close();
	bool IsOpen() const;
	bool Send(const Address& destination,
		const void* data,
		int size);
	int Receive(Address& sender,
		void* data,
		int size,
		bool blocking = true);
	int Available();
	int Error();
private:
	bool open;
	socket_t handle;
};
