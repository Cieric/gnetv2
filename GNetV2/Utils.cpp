#include "Utils.h"
#include <varargs.h>

std::string Format(const char* format, ...)
{
	static char* buffer = nullptr;
	static size_t buffersize = 0;

	va_list argptr;
	__crt_va_start(argptr, format);
	//__va_start(&argptr, 0);
	int size = vsnprintf(NULL, 0, format, argptr);
	snprintf(NULL, 0, format, argptr);
	if (buffersize < size)
	{
		void* temp = realloc(buffer, size);
		if (temp == nullptr) return "";
		buffersize = size;
		buffer = (char*)temp;
	}
	vsprintf(buffer, format, argptr);
	__crt_va_end(argptr);
	return std::string(buffer);
}
