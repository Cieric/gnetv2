#pragma once
#include <string>
#include <chrono>

std::string Format(const char* format, ...);

template<int>
bool LimitFPS(float time)
{
	static auto lastTime = std::chrono::high_resolution_clock::now();
	auto now = std::chrono::high_resolution_clock::now();
	bool flag = false;
	if (std::chrono::duration_cast<std::chrono::duration<float>>(now - lastTime).count() > time)
	{
		flag = true;
		lastTime = now;
	}
	return flag;
}