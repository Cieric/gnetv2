#include <stdio.h>
#include <Platform.h>
#include <Connection.h>
#include <Address.h>
#include <windows.h>
#include <objidl.h>
#include <string>
#include <Utils.h>
#include <thread>

const int port = 7778;
Connection server;

#if PLATFORM != PLATFORM_WINDOWS
#include <unistd.h>
int Sleep(long msec)
{
	struct timespec ts;
	int res;

	if (msec < 0)
	{
		errno = EINVAL;
		return -1;
	}

	ts.tv_sec = msec / 1000;
	ts.tv_nsec = (msec % 1000) * 1000000;

	do {
		res = nanosleep(&ts, &ts);
	} while (res && errno == EINTR);

	return res;
}
#endif

void WriteRotatedText(HDC hdc, std::string text, int x, int y, float rotation)
{
	LOGFONT lf = {};
	lf.lfEscapement = int(rotation * -10);
	HANDLE hFont = CreateFontIndirectA(&lf);
	hFont = (HFONT)SelectObject(hdc, hFont);
	TextOutA(hdc, x, y, text.c_str(), (int)text.length());
	hFont = (HFONT)SelectObject(hdc, hFont);
	DeleteObject(hFont);
}

bool WriteText(HDC hdc, int x, int y, std::string text)
{
	return TextOutA(hdc, x, y, text.c_str(), int(text.length()));
}

bool DrawLine(HDC hdc, int x1, int y1, int x2, int y2, HPEN hPen)
{
	return MoveToEx(hdc, x1, y1, NULL) && LineTo(hdc, x2, y2);
}

static float lastBitRate = 0.0;
void OnPaint(HDC hdc)
{
	using namespace std::chrono;
	auto now = high_resolution_clock::now();
	
	SetBkMode(hdc, TRANSPARENT);
	//WriteRotatedText(hdc, "Hello World!", 128, 128, 90.0);

	for (int i = 0; i < server.log.size(); i++) {
		WriteText(hdc, 16, 458 - i * 16, server.log[i]);
	}

	WriteText(hdc, 16, 16, Format("RTT: %lf", server.avgRTT));
	WriteText(hdc, 16, 32, Format("Connected: %d", server.Connected()));
	WriteText(hdc, 16, 48, Format("Bitrate: %0.1f MB/s", lastBitRate/1000));

	//static auto lb = LOGBRUSH{ 0, RGB(255, 0, 0), 0 };
	//static auto hPen = ExtCreatePen(PS_GEOMETRIC | PS_ENDCAP_FLAT, 16, &lb, 0, NULL);
	//auto oldpen = SelectObject(hdc, hPen);
	//int limit = 16;
	//for (int i = 0; i < limit; i++)
	//{
	//	auto then = server.send_time[(server.localSequence - i + 1024) % 1024];
	//	double span = duration_cast<duration<double, std::milli>>(now - then).count();
	//	int x = 488 - (limit * 16);
	//	int y = span;
	//	DrawLine(hdc, x, 0, x, 128, hPen);
	//}
	//SelectObject(hdc, oldpen);
}

float AverageBitRate(float bitrate)
{
	static std::chrono::time_point<std::chrono::steady_clock> oldTime = std::chrono::high_resolution_clock::now();
	static float accumulator;

	accumulator += bitrate;

	if (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - oldTime) >= std::chrono::seconds{ 1 }) {
		oldTime = std::chrono::high_resolution_clock::now();
		lastBitRate = accumulator;
		accumulator = 0;
	}

	return lastBitRate;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message,
	WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
	case WM_PAINT:
		HDC          hdc;
		PAINTSTRUCT  ps;
		hdc = BeginPaint(hWnd, &ps);
		OnPaint(hdc);
		EndPaint(hWnd, &ps);
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
} // WndProc

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, PSTR, INT iCmdShow)
{
	HWND                hWnd;
	MSG                 msg;
	WNDCLASS            wndClass;

	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = TEXT("GettingStarted");

	RegisterClass(&wndClass);

	hWnd = CreateWindow(
		TEXT("GettingStarted"),   // window class name
		TEXT("Getting Started"),  // window caption
		WS_OVERLAPPEDWINDOW,      // window style
		CW_USEDEFAULT,            // initial x position
		CW_USEDEFAULT,            // initial y position
		512,		              // initial x size
		512,		              // initial y size
		NULL,                     // parent window handle
		NULL,                     // window menu handle
		hInstance,                // program instance handle
		NULL);                    // creation parameters

	HBRUSH brush = CreateSolidBrush(RGB(200, 200, 200));
	SetClassLongPtr(hWnd, GCLP_HBRBACKGROUND, (LONG_PTR)brush);

	ShowWindow(hWnd, iCmdShow);
	UpdateWindow(hWnd);

	InitializeSockets();

	//Address ServerAddress = Address(99, 105, 206, 85, port);
	Address ServerAddress = Address(127, 0, 0, 1, port);

	if (!server.Connect(ServerAddress))
	{
		printf("CLIENT: failed to create socket!\n");
		return 1;
	}
	
	bool error = false;

	auto fDone = false;
	while (!fDone)
	{
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);

			if (msg.message == WM_QUIT)
			{
				fDone = true;
				error = true;
				break;
			}
		}
		if (LimitFPS<__COUNTER__>(1.f / 30.f))
		{
			InvalidateRect(hWnd, 0, TRUE);
		}

		if (server.Connected() && !error)
		{
			unsigned char buffer[256];
			int bytes_read = 0;
			float bitrate = 0.0f;
			do {
				bytes_read = server.Receive(buffer, sizeof(buffer), false);
				if (bytes_read < 0) {
					error = true;
					break;
				};
				if (bytes_read > 0)
					bitrate += 256;
			} while (bytes_read > 0);

			AverageBitRate(bitrate);

			if (LimitFPS<__COUNTER__>(1.f / 30.f))
			{
				server.Send("Hello, Server!", 14);
			}
		}
	}

	ShutdownSockets();

	return (int)msg.wParam;
}  // WinMain
