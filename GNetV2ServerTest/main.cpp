#include <stdio.h>
#include <Platform.h>
#include <Connection.h>
#include <Address.h>
#include <windows.h>
#include <string>
#include <Utils.h>

const int port = 7778;

#if PLATFORM != PLATFORM_WINDOWS
#include <unistd.h>
int Sleep(long msec)
{
	struct timespec ts;
	int res;

	if (msec < 0)
	{
		errno = EINVAL;
		return -1;
	}

	ts.tv_sec = msec / 1000;
	ts.tv_nsec = (msec % 1000) * 1000000;

	do {
		res = nanosleep(&ts, &ts);
	} while (res && errno == EINTR);

	return res;
}
#endif


void clear() {
	COORD topLeft = { 0, 0 };
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO screen;
	DWORD written;

	GetConsoleScreenBufferInfo(console, &screen);
	FillConsoleOutputCharacterA(
		console, ' ', screen.dwSize.X * screen.dwSize.Y, topLeft, &written
	);
	//FillConsoleOutputAttribute(
	//	console, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE,
	//	screen.dwSize.X * 3, topLeft, &written
	//);
	SetConsoleCursorPosition(console, topLeft);
}

float AverageBitRate(float bitrate)
{
	static std::chrono::time_point<std::chrono::steady_clock> oldTime = std::chrono::high_resolution_clock::now();
	static float accumulator;
	static float lastBitRate = 0.0;

	accumulator += bitrate;

	if (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - oldTime) >= std::chrono::seconds{ 1 }) {
		oldTime = std::chrono::high_resolution_clock::now();
		lastBitRate = accumulator;
		accumulator = 0;
	}

	return lastBitRate;
}

#define CLIENT

int main(int argc, char** argv)
{
	InitializeSockets();

	Connection client;

	printf("Waiting for client to connect...\n");
	if (!client.Listen(port))
	{
		printf("SERVER: failed to create socket!\n");
		return 1;
	}

	bool error = false;

	while (client.Connected() && !error)
	{
		if (LimitFPS<__COUNTER__>(1. / 90.))
		{
			client.Send("Hello, Client!", 14);
		}
		unsigned char buffer[256];
		int bytes_read = 0;
		float bitrate = 0.0f;

		do {
			bytes_read = client.Receive(buffer, sizeof(buffer), false);
			if (bytes_read < 0) {
				error = true;
				break;
			};
			if (bytes_read > 0)
				bitrate += 256;
		} while (bytes_read > 0);
	}

	printf("Server lost connection to the client!\n");

	ShutdownSockets();

	return 0;
}
