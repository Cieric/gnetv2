mkdir ./tmp/GNetV2/
gcc -Wno-multichar -c GNetV2/Address.cpp -o tmp/GNetV2/Address.o
gcc -Wno-multichar -c GNetV2/Connection.cpp -o tmp/GNetV2/Connection.o
gcc -Wno-multichar -c GNetV2/Socket.cpp -o tmp/GNetV2/Socket.o
ar rvs ./Builds/x86/Debug/libGNetV2.a tmp/GNetV2/Address.o tmp/GNetV2/Connection.o tmp/GNetV2/Socket.o
gcc GNetV2TestPlatform/main.cpp -o Builds/x86/Debug/GNetV2TestPlatform -IGNetV2 -L./Builds/x86/Debug/ -lGNetV2 -Wno-multichar -std=c++11 -lstdc++ -pthread